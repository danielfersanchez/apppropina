﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace apppropina
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            btncalcular.Clicked += Btncalcular_Clicked;
		}

        private void Btncalcular_Clicked(object sender, EventArgs e)
        {
            var total = decimal.Parse(entotal.Text);
            var propina = int.Parse(enpropina.Text);
            var numeropersonas = int.Parse(enpersonas.Text);

            var totalpropina = (total * (propina / 100));

            txttotalpropina.Detail = totalpropina.ToString();
            txtpropinapersona.Detail = (total / numeropersonas).ToString();
            txttotalpersona.Detail = (total + totalpropina / numeropersonas).ToString();
            txttotal.Detail = (total + totalpropina).ToString();
        }
    }
}
