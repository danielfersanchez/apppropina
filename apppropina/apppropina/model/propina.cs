﻿using apppropina.viewmodel;
using System;
using System.Collections.Generic;
using System.Text;

namespace apppropina.model
{
    class propina:Notificable
    {



        private double Total;
        private int Personas;
        private int Totalpersonas;
        private double Totalpropina;
        private double Propinapersona;
        private double procetajepersona;
        private double Totalcuenta;
        #region propiedades

        public double total
        {
            get
            {
                return Total;
            }
            set
            {
                if (Total == value)
                {
                    return;
                }
                Total = value;
                OnPropertyChanged();
            }
        }


        public int personas
        {
            get
            {
                return Personas;
            }
            set
            {
                if (Personas == value)
                {
                    return;
                }
                Personas = value;
                OnPropertyChanged();
            }
        }

        public int totalpersonas
        {
            get
            {
                return Totalpersonas;
            }
            set
            {
                if (Totalpersonas == value)
                {
                    return;
                }
                Totalpersonas = value;
                OnPropertyChanged();
            }
        }

        public double totalpropina
        {
            get
            {
                return Totalpropina;
            }
            set
            {
                if (Totalpropina == value)
                {
                    return;
                }
                Totalpropina = value;
                OnPropertyChanged();
            }
        }
        public double propinapersona
        {
            get
            {
                return Propinapersona;
            }
            set
            {
                if (Propinapersona == value)
                {
                    return;
                }
                Propinapersona = value;
                OnPropertyChanged();
            }
        }
        public double porcentajepropina
        {
            get
            {
                return procetajepersona;
            }
            set
            {
                if (procetajepersona == value)
                {
                    return;
                }
                procetajepersona = value;
                OnPropertyChanged();
            }
        }
        public double totalcuenta
        {
            get
            {
                return Totalcuenta;
            }
            set
            {
                if (Totalcuenta == value)
                {
                    return;
                }
                Totalcuenta = value;
                OnPropertyChanged();
            }
        }
        #endregion


    }
}
